//
//  AppDelegate.h
//  Pong
//
//  Created by Mattias K on 2015-03-02.
//  Copyright (c) 2015 MattiasK. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

