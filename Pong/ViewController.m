//
//  ViewController.m
//  Pong
//
//  Created by Mattias K on 2015-03-02.
//  Copyright (c) 2015 MattiasK. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIView *playerPad;
@property (weak, nonatomic) IBOutlet UIView *ball;
@property (weak, nonatomic) IBOutlet UIButton *startButton;
@property (weak, nonatomic) IBOutlet UILabel *pointLabel;
@property (nonatomic) int point;
@property (nonatomic) float xSpeed;
@property (nonatomic) float ySpeed;
@property (nonatomic) bool gameOn;
@property (nonatomic) NSTimer *timer;
@property (nonatomic) NSTimer *secondTimer;
@property (nonatomic) float xSave;
@property (nonatomic) float ySave;
@property (nonatomic) float xPadSave;
@end

@implementation ViewController
- (IBAction)startGame:(id)sender {
    [self initGame];
    self.startButton.hidden = YES;
    [self startTimer];
    
}
- (IBAction)viewPan:(UIPanGestureRecognizer *)sender {
    CGPoint translation = [sender translationInView:self.view];
    self.playerPad.center = CGPointMake(self.playerPad.center.x + (translation.x*1.1), self.playerPad.center.y);
    [sender setTranslation:CGPointMake(0, 0) inView:self.view];
}

-(void) startTimer {
    self.timer = [NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(moveBall) userInfo:nil repeats:YES];
    self.gameOn = YES;
}

-(void) stopTimer {
    [self.timer invalidate];
    self.timer = nil;
    [self loadGame];
    [self initGame];
    self.secondTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(startTimer) userInfo:nil repeats:NO];
}

-(void) moveBall {
    
    
    if(CGRectIntersectsRect(self.playerPad.frame, self.ball.frame)) {
        if (self.ball.center.x < self.playerPad.center.x) {
            self.xSpeed = fabsf(self.xSpeed) * -1;
        }
        else {
            self.xSpeed = fabsf(self.xSpeed);
        }
        self.ySpeed *= -1;
        
    }
    else if (self.ball.center.x + self.ball.frame.size.width/2 > self.view.frame.size.width || self.ball.center.x - self.ball.frame.size.width /2 < 0) {
        self.xSpeed *= -1;
    }
    else if (self.ball.center.y < 0) {
        self.ySpeed *= -1;
        [self score];
        
    }
    else if (self.ball.center.y > self.view.frame.size.height) {
        self.gameOn = NO;
        [self stopTimer];
        
    }
    self.ball.center = CGPointMake(self.ball.center.x + self.xSpeed, self.ball.center.y + self.ySpeed);
}
-(void) score {
    self.xSpeed *= 1.2;
    self.ySpeed *= 1.2;
    self.point ++;
    self.xSave =self.ball.center.x;
    self.ySave =self.ball.center.y;
    self.xPadSave = self.playerPad.center.x;
    self.pointLabel.text = [NSString stringWithFormat: @"%d",self.point ];

}
-(void) initGame {
    if (!self.gameOn) {
    self.xSpeed = self.view.frame.size.width/200;
    self.ySpeed = self.view.frame.size.height/300;
    
    if (arc4random()%2 == 0){
        self.xSpeed *= -1;
    }
    if (arc4random()%2 ==0) {
        self.ySpeed *= -1;
    }
    
    self.point = 0;
    self.pointLabel.text=@"0";
    }

}

-(void) loadGame {
    if(!self.gameOn){
    self.playerPad.frame = CGRectMake(self.view.center.x, 0, self.view.frame.size.width/6, 10);
    self.ball.frame = CGRectMake(0, 0, self.playerPad.frame.size.width/6, self.playerPad.frame.size.width/6);
    self.ball.center = self.view.center;
    self.playerPad.center = CGPointMake(self.view.center.x, self.view.frame.size.height - self.view.frame.size.height/40);
    }
    if(self.gameOn){
    self.playerPad.frame = CGRectMake(self.xPadSave, 0, self.view.frame.size.width/6, 10);
    self.playerPad.center = CGPointMake(self.xPadSave, self.view.frame.size.height - self.view.frame.size.height/40);
    }
    
}
-(void)viewDidAppear:(BOOL)animated {
    
    [self loadGame];
}
-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    if(self.gameOn){
        
        self.ball.center = CGPointMake(self.xSave + self.xSpeed, self.ySave + self.ySpeed);
    }
    [self loadGame];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.gameOn = NO;
    
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
