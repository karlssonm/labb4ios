//
//  main.m
//  Pong
//
//  Created by Gunilla Lindberg on 2015-03-02.
//  Copyright (c) 2015 MattiasK. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
